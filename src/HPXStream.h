//
// Created by WhiteBlue on 2020-02-17.
//

#pragma once

#include <iostream>
#include <stdexcept>

#include <hpx/include/parallel_copy.hpp>
#include <hpx/include/parallel_fill.hpp>
#include <hpx/include/parallel_transform.hpp>
#include <hpx/include/parallel_executors.hpp>
#include <hpx/include/parallel_executor_parameters.hpp>
#include <hpx/include/iostreams.hpp>
#include <hpx/include/threads.hpp>
#include <hpx/include/compute.hpp>
#include <hpx/include/parallel_transform_reduce.hpp>

#include "Stream.h"

#define IMPLEMENTATION_STRING "HPX"

template<typename T>
class HPXStream : public Stream<T> {
protected:
    hpx::compute::host::block_executor<> *exec;

    // Device side pointers
    hpx::compute::vector<T, hpx::compute::host::block_allocator<T>> *a;
    hpx::compute::vector<T, hpx::compute::host::block_allocator<T>> *b;
    hpx::compute::vector<T, hpx::compute::host::block_allocator<T>> *c;

public:
    HPXStream(const unsigned int);

    ~HPXStream();

    virtual void copy() override;

    virtual void add() override;

    virtual void mul() override;

    virtual void triad() override;

    virtual T dot() override;

    virtual void init_arrays(T initA, T initB, T initC) override;

    virtual void read_arrays(std::vector<T> &a, std::vector<T> &b, std::vector<T> &c) override;

};