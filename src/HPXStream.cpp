//
// Created by WhiteBlue on 2020-02-17.
//


#include "HPXStream.h"

template<typename T>
struct multiply_step {
    multiply_step(T factor) : factor_(factor) {}

    template<typename U>
    HPX_HOST_DEVICE HPX_FORCEINLINE T operator()(U val) const {
        return val * factor_;
    }

    T factor_;
};

template<typename T>
struct add_step {
    template<typename U>
    HPX_HOST_DEVICE HPX_FORCEINLINE T operator()(U val1, U val2) const {
        return val1 + val2;
    }
};

template<typename T>
struct triad_step {
    triad_step(T factor) : factor_(factor) {}

    template<typename U>
    HPX_HOST_DEVICE HPX_FORCEINLINE T operator()(U val1, U val2) const {
        return val1 + val2 * factor_;
    }

    T factor_;
};

template<typename T>
struct dot_step {
    template<typename U>
    HPX_HOST_DEVICE HPX_FORCEINLINE T operator()(U val1, U val2) const {
        return val1 * val2;
    }
};


template<typename T>
HPXStream<T>::HPXStream(const unsigned int ARRAY_SIZE) {
    typedef hpx::compute::host::block_allocator<T> allocator_type;
    typedef hpx::compute::vector<T, allocator_type> vector_type;

    std::vector<hpx::compute::host::target> targets = hpx::compute::host::numa_domains();
    this->exec = new hpx::compute::host::block_executor<>(targets);

    allocator_type alloc(targets);

    this->a = new vector_type(ARRAY_SIZE, alloc);
    this->b = new vector_type(ARRAY_SIZE, alloc);
    this->c = new vector_type(ARRAY_SIZE, alloc);
}

template<typename T>
HPXStream<T>::~HPXStream() {
    free(this->a);
    free(this->b);
    free(this->c);
    free(this->exec);
}

template<typename T>
void HPXStream<T>::init_arrays(T initA, T initB, T initC) {
    auto policy = hpx::parallel::execution::par.on(*this->exec);
    // Initialize arrays
    hpx::parallel::fill(policy, this->a->begin(), this->a->end(), initA);
    hpx::parallel::fill(policy, this->b->begin(), this->b->end(), initB);
    hpx::parallel::fill(policy, this->c->begin(), this->c->end(), initC);
}

template<typename T>
void HPXStream<T>::read_arrays(std::vector<T> &h_a, std::vector<T> &h_b, std::vector<T> &h_c) {
    auto policy = hpx::parallel::execution::par.on(*this->exec);

    hpx::parallel::copy(policy, this->a->begin(), this->a->end(), std::begin(h_a));
    hpx::parallel::copy(policy, this->b->begin(), this->b->end(), std::begin(h_b));
    hpx::parallel::copy(policy, this->c->begin(), this->c->end(), std::begin(h_c));
}

template<typename T>
void HPXStream<T>::copy() {
    auto policy = hpx::parallel::execution::par.on(*this->exec);

    hpx::parallel::copy(policy, this->a->begin(), this->a->end(), this->c->begin());
}

template<typename T>
void HPXStream<T>::mul() {
    auto policy = hpx::parallel::execution::par.on(*this->exec);

    const T scalar = startScalar;

    hpx::parallel::transform(policy, this->c->begin(), this->c->end(), this->b->begin(), multiply_step<T>(scalar));
}

template<typename T>
void HPXStream<T>::add() {
    auto policy = hpx::parallel::execution::par.on(*this->exec);

    hpx::parallel::transform(policy, this->a->begin(), this->a->end(), this->b->begin(), this->b->end(),
                             this->c->begin(), add_step<T>());
}

template<typename T>
void HPXStream<T>::triad() {
    auto policy = hpx::parallel::execution::par.on(*this->exec);

    const T scalar = startScalar;

    hpx::parallel::transform(policy, this->b->begin(), this->b->end(), this->c->begin(), this->c->end(),
                             this->a->begin(), triad_step<T>(scalar));

}

template<typename T>
T HPXStream<T>::dot() {
    auto policy = hpx::parallel::execution::par.on(*this->exec);

    T ret = hpx::parallel::transform_reduce(policy, this->a->begin(), this->a->end(), this->b->begin(), T(0.0),
                                            std::plus<T>(), dot_step<T>());

    return ret;
}

void listDevices() {
    std::cout << "0: CPU" << std::endl;
}

std::string getDeviceName(const int) {
    return std::string("Device name unavailable");
}

std::string getDeviceDriver(const int) {
    return std::string("Device driver unavailable");
}

template
class HPXStream<float>;

template
class HPXStream<double>;
