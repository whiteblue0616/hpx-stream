
// Copyright (c) 2015-16 Tom Deakin, Simon McIntosh-Smith,
// University of Bristol HPC
//
// For full license terms please see the LICENSE file distributed with this
// source code

#include "SYCLStream.h"

#include <iostream>

using namespace cl::sycl;

program *p;

template<class T>
SYCLStream<T>::SYCLStream(const unsigned int ARRAY_SIZE, const int device_index) {
    this->array_size = ARRAY_SIZE;
    this->queue = new cl::sycl::queue();
    // Create buffers
    d_a = new buffer<T>(ARRAY_SIZE);
    d_b = new buffer<T>(ARRAY_SIZE);
    d_c = new buffer<T>(ARRAY_SIZE);
}

template<class T>
SYCLStream<T>::~SYCLStream() {
    delete this->d_a;
    delete this->d_b;
    delete this->d_c;
    delete this->queue;
    delete p;
}

template<class T>
void SYCLStream<T>::copy() {
    this->queue->submit([&](handler &cgh) {
        auto ka = this->d_a->template get_access<access::mode::read>(cgh);
        auto kc = this->d_c->template get_access<access::mode::write>(cgh);
        cgh.parallel_for<copy_kernel>(range<1>{array_size}, [=](id<1> idx) {
            kc[idx] = ka[idx];
        });
    });
    queue->wait();
}

template<class T>
void SYCLStream<T>::mul() {
    const T scalar = startScalar;
    this->queue->submit([&](handler &cgh) {
        auto kb = this->d_b->template get_access<access::mode::write>(cgh);
        auto kc = this->d_c->template get_access<access::mode::read>(cgh);
        cgh.parallel_for<mul_kernel>(range<1>{array_size}, [=](id<1> idx) {
            kb[idx] = scalar * kc[idx];
        });
    });
    queue->wait();
}

template<class T>
void SYCLStream<T>::add() {
    this->queue->submit([&](handler &cgh) {
        auto ka = this->d_a->template get_access<access::mode::read>(cgh);
        auto kb = this->d_b->template get_access<access::mode::read>(cgh);
        auto kc = this->d_c->template get_access<access::mode::write>(cgh);
        cgh.parallel_for<add_kernel>(range<1>{array_size}, [=](id<1> idx) {
            kc[idx] = ka[idx] + kb[idx];
        });
    });
    queue->wait();
}

template<class T>
void SYCLStream<T>::triad() {
    const T scalar = startScalar;
    this->queue->submit([&](handler &cgh) {
        auto ka = this->d_a->template get_access<access::mode::write>(cgh);
        auto kb = this->d_b->template get_access<access::mode::read>(cgh);
        auto kc = this->d_c->template get_access<access::mode::read>(cgh);
        cgh.parallel_for<triad_kernel>(range<1>{array_size}, [=](id<1> idx) {
            ka[idx] = kb[idx] + scalar * kc[idx];
        });
    });
    queue->wait();
}

template<class T>
T SYCLStream<T>::dot() {
    T sum = 0.0;

//    this->queue->submit([&](handler &cgh) {
//        auto ka = this->d_a->template get_access<access::mode::read>(cgh);
//        auto kb = this->d_b->template get_access<access::mode::read>(cgh);
//        cgh.parallel_for<dot_kernel>(range<1>{array_size}, [=](id<1> idx) {
//            sum += ka[idx] * kb[idx];
//        });
//    });


    return sum;
}

template<class T>
void SYCLStream<T>::init_arrays(T initA, T initB, T initC) {
    this->queue->submit([&](handler &cgh) {
        auto ka = this->d_a->template get_access<access::mode::write>(cgh);
        auto kb = this->d_b->template get_access<access::mode::write>(cgh);
        auto kc = this->d_c->template get_access<access::mode::write>(cgh);
        cgh.parallel_for<init_kernel>(range<1>{array_size}, [=](item<1> item) {
            auto id = item.get_id(0);
            ka[id] = initA;
            kb[id] = initB;
            kc[id] = initC;
        });
    });
    queue->wait();
}

template<class T>
void SYCLStream<T>::read_arrays(std::vector<T> &a, std::vector<T> &b, std::vector<T> &c) {
    auto _a = this->d_a->template get_access<access::mode::read>();
    auto _b = this->d_b->template get_access<access::mode::read>();
    auto _c = this->d_c->template get_access<access::mode::read>();
    for (int i = 0; i < array_size; i++) {
        a[i] = _a[i];
        b[i] = _b[i];
        c[i] = _c[i];
    }
}

void listDevices() {
    std::cout << "0: CPU" << std::endl;
}

std::string getDeviceName(const int) {
    return std::string("Device name unavailable");
}

std::string getDeviceDriver(const int) {
    return std::string("Device driver unavailable");
}


// TODO: Fix kernel names to allow multiple template specializations
template
class SYCLStream<float>;

template
class SYCLStream<double>;
