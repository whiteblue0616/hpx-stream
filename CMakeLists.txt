cmake_minimum_required(VERSION 3.15)
project(Stream_HPX)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_CURRENT_SOURCE_DIR}/sycl-cmake")

include(FindTriSYCL)
find_package(HPX REQUIRED)

option(BUILD_SYCL "Build SYCL version" ON)
option(BUILD_HPX "Build HPX version" OFF)
option(BUILD_OMP "Build OMP version" OFF)

set(DIR_SRCS src/main.cpp src/Stream.h)


if (BUILD_HPX)
    add_definitions(-DENABLE_HPX)
    set(DIR_SRCS ${DIR_SRCS} src/HPXStream.cpp src/HPXStream.h)
elseif (BUILD_SYCL)
    add_definitions(-DENABLE_SYCL)
    set(DIR_SRCS ${DIR_SRCS} src/SYCLStream.cpp src/SYCLStream.h)
else ()
    add_definitions(-DENABLE_OMP)
    set(DIR_SRCS ${DIR_SRCS} src/OMPStream.cpp src/OMPStream.h)
endif ()


add_hpx_executable(main
        SOURCES ${DIR_SRCS}
        COMPONENT_DEPENDENCIES iostreams)

add_sycl_to_target(main)